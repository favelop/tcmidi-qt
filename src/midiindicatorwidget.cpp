#include "midiindicatorwidget.h"

#include <QPainter>

MidiIndicatorWidget::MidiIndicatorWidget(QWidget *parent) : QWidget(parent)
{
    setAttribute(Qt::WA_NoSystemBackground);
    setAttribute(Qt::WA_TransparentForMouseEvents);
    setAutoFillBackground(false);

    value = 0;

    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &MidiIndicatorWidget::on_update);
}

void MidiIndicatorWidget::paintEvent(QPaintEvent *) {

    int f = static_cast<int>((value * 1.0 / INDICATOR_VAL_MAX) * 255);
    QPainter(this).fillRect(rect(), {f, f, 0, 128});
}

int MidiIndicatorWidget::heightForWidth( int w ) const { return w; }

void MidiIndicatorWidget::on_indicate() {
    value = INDICATOR_VAL_MAX;
    timer->start(INDICATOR_TIMER);
}

void MidiIndicatorWidget::on_update() {
    value -= INDICATOR_VAL_STEP;

    if (value < 0) {
        value = 0;
        timer->stop();
    }

    update();
}
