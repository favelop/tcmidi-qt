#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "connectdialog.h"
#include "tcmidi.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void disconnect();
    void abort();
    void createHost(quint16);
    void createConnection(QString ipAddress, quint16);

public slots:
    void on_connectButton_clicked();

    void on_update_connection_state(TcMidi::ConnectionState);

    void on_midi_devices_refreshed(QStringList list);
    void on_message(QString message);

private:
    Ui::MainWindow *ui;
    ConnectDialog *dialog = nullptr;
    TcMidi * tc;
    QThread *worker;

    TcMidi::ConnectionState connectionState;
};

#endif // MAINWINDOW_H
