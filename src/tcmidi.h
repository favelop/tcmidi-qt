#ifndef TCMIDI_H
#define TCMIDI_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>
#include <QHostAddress>
#include <QMutex>
#include <QWaitCondition>
#include <QThread>
#include <QVector>

#include "RtMidi.h"


class TcMidi : public QObject
{
    Q_OBJECT
public:
    explicit TcMidi(QObject *parent = nullptr);
    ~TcMidi();

    void sendMidi(std::vector<unsigned char> *messageme);

    enum ConnectionState {
        Disconnected,
        Connecting,
        Connected
    };

signals:
    // connecting?
    // connected
    // disconnected
    // midi devices changed
    // midi event received
    void midiDevicesRefreshed(QStringList list);
    void updateConnectionState(TcMidi::ConnectionState);
    void connected();
    void disconnected();
    void finished(QString reason);
    void messageme(QString messageme);

    void midiSent();
    void midiReceived();
    void midiInput(std::vector<unsigned char> message);

public slots:
    // disconnect
    void on_midi_refresh();
    void on_midi_port_changed(int index);

    void init();

    void on_requested_connect(QString ipAddress, quint16 port);
    void on_requested_host(quint16);
    void on_requested_disconnect();
    void on_requested_abort();

private slots:
    void on_midi_input(std::vector<unsigned char> message);
    void on_connected();
    void on_disconnected();
    void on_new_connection();
    void on_server_error(QAbstractSocket::SocketError socketError);
    void on_incoming_data();

private:

    void finish(QString reason);
    void initSocket();

    void selectMidiDevice(int num);

    QString getPeerAddress();
    QString getSocketPeerAddress();

    void receiveMidi();

    bool done;
    bool isHost;
    bool connecting;
    bool socketConnected;

    QString ipAddress;
    quint16 port;

    QTcpServer *server;
    QTcpSocket *socket;
    QTcpSocket *sendSocket; // multithreaded - sending thread (via callbacks)
    QTcpSocket *recvSocket; // multithreaded - receiving thread (via main)
    QMutex callbackMutex; // for callback thread (new local midi message)

    QWaitCondition waitCondition;
    QMutex waitMutex;

    RtMidiIn *midiin;
    RtMidiOut *midiout;

    // receiving data
    qint64 bytesAvail;
    char* message;
    int messageSize = 0;
    char* buffer = new char[512];
    long long bytesRead = 0;
    int packetSize = 0;
};

Q_DECLARE_METATYPE(TcMidi::ConnectionState);

#endif // TCMIDI_H
