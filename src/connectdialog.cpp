#include "connectdialog.h"
#include "ui_connectdialog.h"

ConnectDialog::ConnectDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConnectDialog)
{
    ui->setupUi(this);

    m_confirmed = false;

    //https://evileg.com/en/post/56/
    /* Create a string for a regular expression */
    QString ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
    /* Create a regular expression with a string
     * as a repeating element
     */
    QRegExp ipRegex ("^" + ipRange
                     + "\\." + ipRange
                     + "\\." + ipRange
                     + "\\." + ipRange + "$");
    /* Create a validation regular expression
     * using a regular expression
     */
    QRegExpValidator *ipValidator = new QRegExpValidator(ipRegex, this);
    /* Set Validator on QLineEdit */
    ui->ipAddressEdit->setValidator(ipValidator);
}

ConnectDialog::~ConnectDialog()
{
    delete ui;
}

QString ConnectDialog::getIpAddress()
{
    return ui->ipAddressEdit->text();
}

quint16 ConnectDialog::getPort()
{
    return static_cast<quint16>(ui->portSpinner->value());
}

bool ConnectDialog::isHost()
{
    return m_isHost;
}
/*
void ConnectDialog::on_ipAddressEdit_textChanged(const QString &newString)
{
    ipAddress = newString;
}

void ConnectDialog::on_portSpinner_valueChanged(int newValue)
{
    port = newValue;
}
*/

void ConnectDialog::on_connectButton_clicked()
{
    m_confirmed = true;
    m_isHost = false;
    this->close();

    emit clickedConnect(getIpAddress(), getPort());
}

void ConnectDialog::on_hostButton_clicked()
{
    m_confirmed = true;
    m_isHost = true;
    this->close();
    emit clickedHost(getPort());
}
