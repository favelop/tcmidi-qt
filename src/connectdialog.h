#ifndef CONNECTDIALOG_H
#define CONNECTDIALOG_H

#include <QDialog>

namespace Ui {
class ConnectDialog;
}

class ConnectDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConnectDialog(QWidget *parent = nullptr);
    ~ConnectDialog();

    QString getIpAddress();
    quint16 getPort();
    bool isHost();
    void reset();
    void wasConfirmed();

signals:
    void clickedConnect(QString, qint16);
    void clickedHost(qint16);

private slots:
    //void on_ipAddressEdit_textChanged(const QString &arg1);

    //void on_portSpinner_valueChanged(int arg1);

    void on_connectButton_clicked();
    void on_hostButton_clicked();

private:
    Ui::ConnectDialog *ui;
    bool m_isHost;
    bool m_confirmed;
};

#endif // CONNECTDIALOG_H
