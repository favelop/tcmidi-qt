#ifndef MIDIINDICATORWIDGET_H
#define MIDIINDICATORWIDGET_H

#include <QWidget>
#include <QTimer>

#define INDICATOR_VAL_MAX 1000
#define INDICATOR_VAL_STEP 100
#define INDICATOR_TIMER 50

class MidiIndicatorWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MidiIndicatorWidget(QWidget *parent = nullptr);

    int heightForWidth(int w) const override;

signals:

public slots:
    void on_indicate();
    void on_update();

protected:
    void paintEvent(QPaintEvent *) override;

private:
    qint16 value;
    QTimer* timer;
};

#endif // MIDIINDICATORWIDGET_H
