#-------------------------------------------------
#
# Project created by QtCreator 2021-08-24T14:57:58
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tcmidi
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    connectdialog.cpp \
    tcmidi.cpp \
    midiindicatorwidget.cpp

HEADERS += \
        mainwindow.h \
    connectdialog.h \
    tcmidi.h \
    midiindicatorwidget.h

FORMS += \
        mainwindow.ui \
    connectdialog.ui

SOURCES += ../rtmidi/RtMidi.cpp
INCLUDEPATH += ../rtmidi

unix:!macx {
    LIBS += -lasound -lpthread
    QMAKE_CXXFLAGS += -D__LINUX_ALSA__
}

macx {
    LIBS += -framework CoreMIDI -framework CoreAudio -framework CoreFoundation
    QMAKE_CXXFLAGS += -D__MACOSX_CORE__
}

win32 {
    # for win32 compilation see https://www.music.mcgill.ca/~gary/rtmidi/
    error(Configuration failed! Please set your compiler flags manually. For win32 compilation see https://www.music.mcgill.ca/~gary/rtmidi/)
}

# prepare RtMidi
#system(cd ../rtmidi && ./autogen.sh)
#system(cd ../rtmidi && ./configure)

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
