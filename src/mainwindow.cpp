#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "connectdialog.h"
#include "tcmidi.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    if (dialog == nullptr) dialog = new ConnectDialog(this);

    tc = new TcMidi();
    qRegisterMetaType<TcMidi::ConnectionState>("ConnectionState");

    // connect signals
    connect(this, &MainWindow::disconnect, tc, &TcMidi::on_requested_disconnect);
    connect(this, &MainWindow::abort, tc, &TcMidi::on_requested_abort);

    connect(dialog, &ConnectDialog::clickedConnect, tc, &TcMidi::on_requested_connect);
    connect(dialog, &ConnectDialog::clickedHost, tc, &TcMidi::on_requested_host);

    connect(ui->refreshButton, &QPushButton::clicked, tc, &TcMidi::on_midi_refresh);
    connect(ui->midiDeviceComboBox, SIGNAL(currentIndexChanged(int)), tc, SLOT(on_midi_port_changed(int)));

    connect(tc, &TcMidi::midiDevicesRefreshed, this, &MainWindow::on_midi_devices_refreshed);
    connect(tc, &TcMidi::messageme, this, &MainWindow::on_message);
    connect(tc, &TcMidi::updateConnectionState, this, &MainWindow::on_update_connection_state);

    connect(tc, &TcMidi::midiSent, ui->outMidiActivity, &MidiIndicatorWidget::on_indicate);
    connect(tc, &TcMidi::midiReceived, ui->inMidiActivity, &MidiIndicatorWidget::on_indicate);

    worker = new QThread;
    tc->moveToThread(worker);
    connect(worker, SIGNAL(started()), tc, SLOT(init()));
    worker->start();
}

MainWindow::~MainWindow()
{
    delete dialog;
    delete ui;
}

void MainWindow::on_midi_devices_refreshed(QStringList list)
{
    ui->midiDeviceComboBox->clear();
    ui->midiDeviceComboBox->addItems(list);
}

void MainWindow::on_message(QString message)
{
    qDebug() << message;
    ui->statusBar->showMessage(message);
}

void MainWindow::on_update_connection_state(TcMidi::ConnectionState state) {
    connectionState = state;

    switch (state) {
        case TcMidi::ConnectionState::Disconnected:
            ui->connectButton->setEnabled(true);
            ui->connectButton->setText("Connect");
            break;
        case TcMidi::ConnectionState::Connected:
            ui->connectButton->setEnabled(true);
            ui->connectButton->setText("Disconnect");
            break;
        case TcMidi::ConnectionState::Connecting:
            ui->connectButton->setEnabled(true);
            ui->connectButton->setText("Abort");
            break;
    }
}

// TODO combine to cancel?
void MainWindow::on_connectButton_clicked()
{
    switch (connectionState) {
        case TcMidi::ConnectionState::Disconnected:
            dialog->show();
            break;
        case TcMidi::ConnectionState::Connected:
            emit disconnect();
            break;
        case TcMidi::ConnectionState::Connecting:
            emit abort();
            break;
    }
}
