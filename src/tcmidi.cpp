#include "tcmidi.h"
#include <QTimer>
#include <QDebug>

//QMutex callbackMutex; // for callback thread (new local midi message)
void send_midi_callback(double deltatime, std::vector<unsigned char> *message, void *userData) {
    TcMidi* that = static_cast<TcMidi*>(userData);
    that->sendMidi(message);
}

TcMidi::TcMidi(QObject *parent) : QObject(parent)
{
    done = false;
    isHost = true;

    socket = nullptr;

    /*QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(on_midi_refresh()));
    timer->start(2000);*/
}

TcMidi::~TcMidi() {
    delete midiin;
    delete midiout;
}


void TcMidi::finish(QString reason) {
    done = true;
    qDebug() << reason;
    emit finished(reason);
}

Q_DECLARE_METATYPE(std::vector<unsigned char>);
void TcMidi::init()
{
    emit updateConnectionState(TcMidi::ConnectionState::Disconnected);

    qRegisterMetaType<std::vector<unsigned char>>("vector");
    connect(this, &TcMidi::midiInput, this, &TcMidi::on_midi_input);

    midiin = new RtMidiIn();
    midiin->setCallback(&send_midi_callback, static_cast<void*>(this));
    midiout = new RtMidiOut(RtMidi::Api::UNSPECIFIED, "tcmidi");
    midiout->openVirtualPort("Remote MIDI Port");

    on_midi_refresh();

}

void TcMidi::on_midi_refresh()
{
    QStringList list;
    // Check available ports.
    unsigned int nPorts = midiin->getPortCount();
    //std::cout << "\nThere are " << nPorts << " MIDI input sources available.\n";
    for ( unsigned int i=0; i<nPorts; i++ ) {
        try {
            list.append(QString::fromStdString(midiin->getPortName(i)));
        }
        catch ( RtMidiError &error ) {
            error.printMessage();
            //return -1;
        }
        qDebug() << "  Input Port #" << i << ": " << list.at(static_cast<int>(i));
        //std::cout << "  Input Port #" << i << ": " << portName << '\n';
    }

    emit midiDevicesRefreshed(list);
}

void TcMidi::on_midi_port_changed(int index)
{
    if (midiin->isPortOpen()) {
            midiin->closePort();
    }
    if (index != -1) {
        midiin->openPort(static_cast<unsigned int>(index));

        // Ignore sysex, Don't ignore timing, or active sensing messages.
        midiin->ignoreTypes(true, false, false );

        qDebug() << "Reading MIDI from port " << index;

    }
}

void TcMidi::on_incoming_data() {
    // new midi data waits on network device we read it
    // we block until thre is one full message
    // begin
    bytesAvail = socket->bytesAvailable();

    if (bytesAvail > 0) {
        bytesRead = 0;

        // recv message size
        while(bytesRead < 1) {
            bytesRead = socket->read(buffer, 1);
        }
        packetSize = buffer[0];

        // recv message
        // keep waiting until enough bytes ready of message size
        while (bytesAvail < packetSize - 1) {
            socket->waitForReadyRead();
            bytesAvail = socket->bytesAvailable();
        }

        bytesRead += socket->read(&buffer[1], packetSize - 1); // sub one because we have read one

        // at this point there must be enough bytes available as packet size
        while (bytesRead < packetSize) {
            bytesRead += socket->read(&buffer[bytesRead], packetSize - 1 - bytesRead); // sub what we have read
        }
        // skip size information for actual MIDI message
        message = &buffer[1];
        messageSize = packetSize - 1;
        //print_midi_message((unsigned char*)message, messageSize);

        // MIDI
        midiout->sendMessage((const unsigned char*)message, (int) messageSize);
        emit midiReceived();
    }
}

void TcMidi::on_midi_input(std::vector<unsigned char> message) {
    // thread safe to access socket now
    // TODO fix segafault
    if (socket != nullptr && socket->state() == QAbstractSocket::SocketState::ConnectedState) {
        socket->write((char*) &message[0], message.size());
        emit midiSent();
    }
}

void TcMidi::sendMidi(std::vector<unsigned char> *message)
{
    message->insert(message->begin(), message->size() + 1);
    emit midiInput(*message); // copy
}

QString TcMidi::getPeerAddress()
{
    QString peer = ipAddress + ":" + QString::number(port);
    qDebug() << peer;
    return peer;
}

QString TcMidi::getSocketPeerAddress()
{
    QString peer = socket->peerAddress().toString() + ":" + QString::number(socket->peerPort());
    qDebug() << peer;
    return peer;
}

void TcMidi::initSocket() {
    connect(socket, SIGNAL(readyRead()), this, SLOT(on_incoming_data()));
    connect(socket, SIGNAL(connected()), this, SLOT(on_connected()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(on_disconnected()));
    socket->setSocketOption(QAbstractSocket::LowDelayOption, 1);
}

void TcMidi::on_requested_connect(QString ipAddress, quint16 port)
{
    this->ipAddress = ipAddress;
    this->port = port;
    isHost = false;

    emit messageme("Connecting to host " + this->getPeerAddress());
    emit updateConnectionState(TcMidi::ConnectionState::Connecting);

    socket = new QTcpSocket(this);
    initSocket();

    // socket for receiving
    if (ipAddress.length() == 0) {
        socket->connectToHost(QHostAddress::LocalHost, port);
    } else {
        socket->connectToHost(ipAddress, port);
    }
}

void TcMidi::on_connected() {
    emit messageme("Connected to " + getSocketPeerAddress() + "");
    emit updateConnectionState(TcMidi::ConnectionState::Connected);
}

void TcMidi::on_disconnected() {
    emit messageme("Disconnected: " + socket->errorString());
    emit updateConnectionState(TcMidi::ConnectionState::Disconnected);
    socket->deleteLater(); // we don't need anymore
    //server->deleteLater(); // we don't need anymore
}

void TcMidi::on_requested_host(quint16 port) {
    isHost = true;
    this->port = port;

    emit messageme("Waiting for peer to connect...");
    emit updateConnectionState(TcMidi::ConnectionState::Connecting);

    server = new QTcpServer(this);
    connect(server, SIGNAL(newConnection()), this, SLOT(on_new_connection()));
    connect(server, &QTcpServer::acceptError, this, &TcMidi::on_server_error);

    if (!server->listen(QHostAddress::Any, port)) {
        finish("Error starting server: " + server->errorString());
    }
}

void TcMidi::on_new_connection() {
    socket = server->nextPendingConnection();
    initSocket();

    on_connected(); // we call manually
}

void TcMidi::on_server_error(QAbstractSocket::SocketError socketError) {
    server->deleteLater(); // we don't need anymore
    emit messageme("Error accepting connection: " + QString::number(socketError));
}

void TcMidi::on_requested_abort()
{
    if (isHost) {
        server->close();
        server->deleteLater();
    } else if (socket->state() == QAbstractSocket::ConnectingState) {
        socket->abort();
        socket->deleteLater();
    }

    // TODO better nullcheck
    socket = nullptr;

    emit updateConnectionState(TcMidi::ConnectionState::Disconnected);
    emit messageme("Aborted.");
}

void TcMidi::on_requested_disconnect()
{
    socket->close();
    // will call emit disconnect and delete them
}
