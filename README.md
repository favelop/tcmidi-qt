# tcmidi-qt

![tcmidi Screenshot](tcmidi_dark.png)

A Qt GUI application to send MIDI events over TCP.

## Building

Get the source code with ``git clone --recurse-submodules git@codeberg.org:favelop/tcmidi-qt.git`` (or replace with HTTPS clone).

This will also get the RtMidi library as a dependency.
You will need to configure it:
```
cd tcmidi-qt/rtmidi
./autogen.sh
./configure
```

After that you are ready to build:
```
cd ..
mkdir build
cd build
qmake ../src/tcmidi.pro
make
```

for Debug:
```
qmake CONFIG=+debug ../src/tcmidi.pro
```

## License

GPL v3
